 * Click Test to execute the tests
 * Fix the test errors by modifying the values in the table
 * View the code of the ConcatFixture in IntelliJ
 * Modify the code of the fixture to concat using ", " instead of " " and press ctrl + F9 to compile it 
 * Rerun the test again in Fitnesse
 * Fix the tests

!include -setup  .GradleClasspath
!|eg.ConcatFixture|
|firstPart|secondPart|together?|totalLength?|
|Hello|World|Hello World|10|
|Houston|We Have a Problem|Houston We Have do not have a Problem|30|
|Add|some more|Add some more|2|
