package nl.jdriven.analyzer;

import nl.jdriven.WishListItem;

/**
 * Created by rb on 02/12/15.
 */
public class AlwaysBuyMeAnalyzer implements  WishListItemAnalyzer {
    @Override
    public boolean shouldIBuyIt(WishListItem wishListItem) {
        return true;
    }
}
