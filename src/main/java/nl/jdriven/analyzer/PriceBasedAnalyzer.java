package nl.jdriven.analyzer;

import nl.jdriven.WishListItem;

public class PriceBasedAnalyzer implements WishListItemAnalyzer {

    final int priceInCents;

    public PriceBasedAnalyzer(int priceInCents) {
        this.priceInCents = priceInCents;
    }

    @Override
    public boolean shouldIBuyIt(WishListItem wishListItem) {
        return wishListItem.getPriceInCents() < priceInCents;
    }
}
