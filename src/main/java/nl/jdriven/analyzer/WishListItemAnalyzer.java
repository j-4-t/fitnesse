package nl.jdriven.analyzer;

import nl.jdriven.WishListItem;

/**
 * Created by rb on 02/12/15.
 */
public interface WishListItemAnalyzer {
    boolean shouldIBuyIt(WishListItem wishListItem);
}
