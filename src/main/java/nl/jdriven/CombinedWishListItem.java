package nl.jdriven;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rb on 02/12/15.
 */
public class CombinedWishListItem extends WishListItem {

    List<WishListItem> items = new ArrayList<>();

    public void add(WishListItem item) {
        items.add(item);
    }

    @Override
    public int getPriceInCents() {
        return items.stream().mapToInt(WishListItem::getPriceInCents).sum();
    }
}
