package nl.jdriven;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rrijnberk on 09/11/15.
 */
public class WishListItem {
    private Map<String, String> attributes = new HashMap<String, String>();
    private String name;
    private int priceInCents;
    private WishListType type;

    public WishListItem(WishListType type) {
        this.type = type;
    }

    public WishListItem() {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WishListItem that = (WishListItem) o;

        if (priceInCents != that.priceInCents) return false;
        return !(name != null ? !name.equals(that.name) : that.name != null);

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + priceInCents;
        return result;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPriceInCents(int priceInCents) {
        this.priceInCents = priceInCents;
    }

    public String getName() {
        return name;
    }

    public int getPriceInCents() {
        return priceInCents;
    }

    public WishListType getType() {
        return type;
    }

    public void setType(WishListType type) {
        this.type = type;
    }

    public void addAttribute(String key, String value) {
        attributes.put(key, value);
    }

    public String getAttribute(String key) {
        return attributes.get(key);
    }

    @Override
    public String toString() {
        Object[] keys = attributes.keySet().toArray();
        int count = 0;
        String result = this.name + "\n";
        result += "- price:\t" + this.priceInCents;
        for(count = 0; count < keys.length; count++) {
            result += "\n- " + keys[count].toString() + ":\t" + attributes.get(keys[count]);
        }
        return result;
    }


}
