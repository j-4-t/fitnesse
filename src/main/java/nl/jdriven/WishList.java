package nl.jdriven;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by rrijnberk on 09/11/15.
 */
public class WishList {
    private List<WishListItem> items = new ArrayList<>();
    private String name;
    private Date created;
    private String owner;

    public WishList() {
        this.created = new Date();
    }

    public WishList(String name) {
        this();
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getName() {
        return name;
    }

    public Date getCreated() {
        return created;
    }

    public String getOwner() {
        return owner;
    }

    public void addItem(WishListItem item) {
        items.add(item);
    }

    public int size() {
        return items.size();
    }

    public void addItems(WishListItem ... wishListItems) {
        for(WishListItem wishListItem : wishListItems) {
            items.add(wishListItem);
        }
    }


    public int totalPriceInCents() {
        int price = 0;
        for(WishListItem item : items) {
            price += item.getPriceInCents();
        }
        return price;
    }

    public int avaragePriceInCents() {
        return totalPriceInCents() / items.size();
    }
}
