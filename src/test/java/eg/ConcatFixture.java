package eg;

import fit.ColumnFixture;

public class ConcatFixture extends ColumnFixture {

    public ConcatFixture() {
        super();
    }

    public String firstPart;
    public String secondPart;
    private int length;

    public String together() {
        length = firstPart.length() + secondPart.length();
        return firstPart + " " + secondPart;
    }

    public int totalLength() {
        return length;
    }

    public String getFirstPart() {
        return firstPart;
    }

    public void setFirstPart(String firstPart) {
        this.firstPart = firstPart;
    }

    public String getSecondPart() {
        return secondPart;
    }

    public void setSecondPart(String secondPart) {
        this.secondPart = secondPart;
    }
}