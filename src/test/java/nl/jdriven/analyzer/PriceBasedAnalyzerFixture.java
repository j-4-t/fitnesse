package nl.jdriven.analyzer;

import fit.ColumnFixture;
import nl.jdriven.WishListItem;

public class PriceBasedAnalyzerFixture extends ColumnFixture {

    private String itemName;

    private int itemPriceInCents;

    private int maxPriceInCents;

    public String getItemName() {
        return itemName;
    }

    public int getItemPriceInCents() {
        return itemPriceInCents;
    }

    public int getMaxPriceInCents() {
        return maxPriceInCents;
    }

    public boolean shouldIBuyIt() {
        WishListItem wishListItem = new WishListItem();
        wishListItem.setName(itemName);
        wishListItem.setPriceInCents(itemPriceInCents);

        WishListItemAnalyzer analyzer = new PriceBasedAnalyzer(maxPriceInCents);
        return analyzer.shouldIBuyIt(wishListItem);
    }
}
